import React from 'react';

// Create a Context object
const UserContext = React.createContext();

// Provider component that allows consuming components to subscribe to context changes
export const UserProvider = UserContext.Provider;

export default UserContext;

/*

UserContext {
	user = 12j89fj128j2
	Provider: function(states, functions)
}

*/