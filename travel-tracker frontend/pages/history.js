import React, { useState, useEffect, useContext, useRef } from 'react';
import Head from 'next/head';
import { Row, Col, Table, Alert } from 'react-bootstrap';
import moment from 'moment';

// import StreetNavigation from '../components/StreetNavigation';
/*import dynamic from 'next/dynamic';
const DynamicComponent = dynamic(() => import('../components/StreetNavigation'), {ssr: false});*/

import mapboxgl from 'mapbox-gl';
mapboxgl.accessToken = process.env.NEXT_PUBLIC_REACT_APP_MAPBOX_KEY;
// import MapboxDirections from '@mapbox/mapbox-gl-directions/dist/mapbox-gl-directions';
import UserContext from '../UserContext';

export default function History(){
	
	const { user } = useContext(UserContext);
	
	const mapContainerRef = useRef(null);
	const [lat, setLat] = useState(14.63289);
	const [long, setLong] = useState(121.04382);

	const [trips, setTrips] = useState([]);

	let tripEntries = [];

	useEffect( () => {

		fetch(`${process.env.NEXT_PUBLIC_BASE_URL}/api/users/details`, {
			method: 'GET',
			headers: {
				'Authorization': `Bearer ${localStorage.getItem('token')}`
				// 'Content-Type': 'application/json'
			},
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);
			setTrips(data.travels);
			
		})

	}, [])

	// console.log(trips);

	tripEntries = trips.map(trip => {
		
		return(
			<tr key={trip._id}>
				<td onClick = {e => setCoordinates(e)}>{trip.origin.longitude}, {trip.origin.latitude}</td>
				<td onClick = {e => setCoordinates(e)}>{trip.destination.longitude}, {trip.destination.latitude}</td>
				<td>{moment(trip.date).format('ll')}</td>
				<td>{Math.round(trip.distance)}</td>
				<td>{Math.round(trip.duration/60)}</td>
			</tr>
		)

	})

	function setCoordinates(e) {
		console.log(e.target.innerHTML);
		const coordinates = (e.target.innerHTML).split(', ');
		setLong(Number(coordinates[0]));
		setLat(Number(coordinates[1]));
	}

	useEffect( () => {

		// instantiate a new mapbox Map object
		const map = new mapboxgl.Map({
			container: mapContainerRef.current,
			style: 'mapbox://styles/mapbox/streets-v11',
			center: [long, lat],
			zoom: 17
		})

		// Add zoom (+/-) buttons
		map.addControl(new mapboxgl.NavigationControl(), 'bottom-right');

		// Create marker centered at the designated coordinates
		const marker = new mapboxgl.Marker()
		.setLngLat([long, lat])
		.addTo(map)

		return () => map.remove()

	}, [lat, long])

	return (
		<React.Fragment>
			<Head>
				<title>Travel History</title>
			</Head>

			<Row>
				<Col xs={12} md={5}>
					<Table size='md'>
						<thead>
							<tr>
								<th>Origin</th>
								<th>Destination</th>
								<th>Date</th>
								<th>Distance (m)</th>
								<th>Duration (mins)</th>
							</tr>
						</thead>

						<tbody>
							{tripEntries}	
						</tbody>

					</Table>
				</Col>

				<Col xs={12} md={7}>
					<div className="mapContainer" ref={mapContainerRef} />
				</Col>

			</Row>

		</React.Fragment>
	)

}